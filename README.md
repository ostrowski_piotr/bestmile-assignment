# Bestmile Asignment
### Piotr Ostrowski

## Unfinished parts and bits
- There is a chance that in some edge cases my code may run into race condition. This might happen while checking the limit of currently running vehicles. I am aware that I should synchronize access to appropriate fields but I ran out of time and I couldn't test that scenario properly
- Some minor UI adjustments like displaying progress indicator on load
- Small number of unit tests. I wish I've added more but again I ran out of time
- Synchronization mechanism could be more sophisticated

## Positive feedback
Well, it was really fun and quite challenging task! Even thought I have a lot of experience with Google Maps I've never had a chance to use Directions API. Now I know how many possibilities opens up when using that one.
Choosing the right architecture was not so obvious and, in my opinion, it was the most challenging thing. Google maps API isn't very friendly when it comes to separating business logic from presentation layer.

## Negative feedback
Very, very time consuming assignment. I devoted a lot of time and I feel that I could spend twice as much to polish the app out

# 04.2020 Update
I've managed to refactor the codebase slightly by replacing dagger2 with koin 2.x and mockito testing framework with mockk.
