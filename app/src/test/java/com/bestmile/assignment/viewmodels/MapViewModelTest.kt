package com.bestmile.assignment.viewmodels

import com.bestmile.assignment.RxImmediateSchedulerRule
import com.bestmile.assignment.di.modules.appModule
import com.bestmile.assignment.di.modules.dataModule
import com.bestmile.assignment.persistance.BestmileDao
import com.bestmile.assignment.persistance.models.StationModel
import com.bestmile.assignment.views.map.MapMarkerSelection
import com.bestmile.assignment.views.map.MapViewModel
import io.mockk.every
import io.reactivex.Single
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.test.KoinTestRule

/**
 * Created by Piotr Ostrowski on 28/11/2018.
 */
class MapViewModelTest : KoinComponent {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(appModule, dataModule)
    }

    private val bestmileDao by inject<BestmileDao>()
    private val mockWebServer by inject<MockWebServer>()

    private lateinit var mapViewModel: MapViewModel

    @Before
    fun setup() {
        mapViewModel = MapViewModel()
    }

    @Test
    fun testMarkerSelected() {
        val testObserver = mapViewModel.stationMarkerToggleObservable().test()
        mapViewModel.onMarkerClick(1)
        testObserver.assertValueCount(1)
        testObserver.assertValue(MapMarkerSelection(1, true))
    }

    @Test
    fun testMarkerChanged() {
        val testObserver = mapViewModel.stationMarkerToggleObservable().test()
        mapViewModel.onMarkerClick(1)
        mapViewModel.onMarkerClick(2)
        testObserver.assertValueCount(3)
        testObserver.assertValues(
            MapMarkerSelection(1, true),
            MapMarkerSelection(1, false),
            MapMarkerSelection(2, true)
        )
    }

    @Test
    fun testStationMarkersDisplay() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(200)
                .setBody("{\"stations\":[{\"uid\":1, \"latitude\": 50.000, \"longitude\": 20.000}]}")
        )
        every { bestmileDao.getAllStations() } returns Single.just(
            listOf(StationModel(1, 50.0, 20.0, "Test"))
        )

        val testObserver = mapViewModel.stationMarkersObservable().test()
        mapViewModel.onMapReady(false)
        testObserver.assertValue { stations -> stations.count() == 1 }
    }

    @Test
    fun testStationsSyncFailed() {
        every { bestmileDao.getAllStations() } returns Single.just(
            listOf(StationModel(1, 50.0, 20.0, "Test"))
        )
        mockWebServer.enqueue(MockResponse().setResponseCode(500))

        val stationsObserver = mapViewModel.stationMarkersObservable().test()
        val alertObserver = mapViewModel.displayErrorObservable().test()
        mapViewModel.onMapReady(false)
        stationsObserver.assertValueCount(0)
        alertObserver.assertValueCount(1)
    }
}
