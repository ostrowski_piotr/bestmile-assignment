package com.bestmile.assignment.di.modules

import com.bestmile.assignment.managers.DataSyncManager
import com.bestmile.assignment.managers.DataSyncManagerImpl
import com.bestmile.assignment.managers.VehicleManager
import com.bestmile.assignment.managers.VehicleManagerImpl
import com.bestmile.assignment.network.BestmileRetrofitClient
import com.bestmile.assignment.network.DirectionsRetrofitClient
import com.bestmile.assignment.persistance.BestmileDao
import io.mockk.mockk
import okhttp3.mockwebserver.MockWebServer
import org.koin.dsl.module

val dataModule = module {

    single { MockWebServer() }

    single {
        mockk<BestmileDao>(relaxed = true)
    }

    single<BestmileRetrofitClient> {
        object : BestmileRetrofitClient() {
            override fun provideBaseURL() = get<MockWebServer>().url("/").toString()
        }
    }

    factory { get<BestmileRetrofitClient>().getStationsService() }

    single<DataSyncManager> {
        DataSyncManagerImpl(
            stationsService = get(),
            bestmileDao = get()
        )
    }

    single { DirectionsRetrofitClient() }

    factory { get<DirectionsRetrofitClient>().getDirectionsService() }

    single<VehicleManager> {
        VehicleManagerImpl(
            bestmileDao = get(),
            directionsService = get(),
            markersColorUtils = get()
        )
    }
}
