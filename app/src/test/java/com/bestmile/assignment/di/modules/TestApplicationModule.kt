package com.bestmile.assignment.di.modules

import com.bestmile.assignment.utils.MarkersColorUtils
import io.reactivex.disposables.CompositeDisposable
import org.koin.dsl.module

/**
 * Created by Piotr Ostrowski on 02/12/2018.
 */
val appModule = module {

    factory { MarkersColorUtils() }

    factory { CompositeDisposable() }
}
