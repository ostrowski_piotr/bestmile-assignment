package com.bestmile.assignment.managers

import com.bestmile.assignment.RxImmediateSchedulerRule
import com.bestmile.assignment.network.models.*
import org.junit.Rule
import org.junit.Test

/**
 * Created by Piotr Ostrowski on 29/11/2018.
 */
class VehicleTest {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @Test
    fun testVehicleMovement() {
        val directionsModel = DirectionsModel(
            "OK", listOf(
                RouteModel(
                    "Summary", listOf(
                        LegsModel(
                            LocationModel(40.0, 20.0),
                            LocationModel(41.0, 21.0),
                            listOf(
                                StepsModel(LocationModel(10.0, 10.0), LocationModel(11.0, 11.0), PolylineModel("1")),
                                StepsModel(LocationModel(20.0, 20.0), LocationModel(21.0, 21.0), PolylineModel("2")),
                                StepsModel(LocationModel(30.0, 30.0), LocationModel(31.0, 31.0), PolylineModel("3")),
                                StepsModel(LocationModel(40.0, 40.0), LocationModel(41.0, 41.0), PolylineModel("4"))
                            )
                        )
                    ), PolylineModel("empty")
                )
            )
        )
        val vehicle = Vehicle(1, 1, directionsModel)
        val observable = vehicle.getMovementObservable().test()
        observable.assertValueAt(0) {
            it.currentPosition.latitude == 10.0 && it.currentPosition.longitude == 10.0
        }
        observable.assertValueAt(3) {
            it.currentPosition.latitude == 40.0 && it.currentPosition.longitude == 40.0
        }
        observable.assertValueCount(4)
        observable.assertComplete()
    }
}
