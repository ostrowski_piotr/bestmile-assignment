package com.bestmile.assignment.managers

import com.bestmile.assignment.RxImmediateSchedulerRule
import com.bestmile.assignment.di.modules.appModule
import com.bestmile.assignment.di.modules.dataModule
import com.bestmile.assignment.persistance.BestmileDao
import io.mockk.verify
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Rule
import org.junit.Test
import org.koin.core.KoinComponent
import org.koin.core.inject
import org.koin.test.KoinTestRule

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
class DataSyncManagerTest : KoinComponent {

    @get:Rule
    val rxImmediateSchedulerRule = RxImmediateSchedulerRule()

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        modules(appModule, dataModule)
    }

    private val mockWebServer by inject<MockWebServer>()
    private val dataSyncManager by inject<DataSyncManager>()
    private val bestmileDao by inject<BestmileDao>()

    @Test
    fun testBasicSynchronization() {
        mockWebServer.enqueue(
            MockResponse().setResponseCode(200)
                .setBody("{\"stations\":[{\"uid\":1, \"latitude\": 50.000, \"longitude\": 20.000}]}")
        )

        dataSyncManager.synchronizeStations().blockingAwait()
        verify(exactly = 1) {
            bestmileDao.insertStations(any())
        }
    }
}
