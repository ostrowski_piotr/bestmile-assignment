package com.bestmile.assignment.di.modules

import androidx.room.Room
import com.bestmile.assignment.managers.DataSyncManager
import com.bestmile.assignment.managers.DataSyncManagerImpl
import com.bestmile.assignment.managers.VehicleManager
import com.bestmile.assignment.managers.VehicleManagerImpl
import com.bestmile.assignment.network.BestmileRetrofitClient
import com.bestmile.assignment.network.DirectionsRetrofitClient
import com.bestmile.assignment.persistance.BestmileDatabase
import org.koin.dsl.module

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
val dataModule = module {
    single {
        Room.databaseBuilder(
            get(),
            BestmileDatabase::class.java,
            BESTMILE_DATABASE
        ).build()
    }

    factory { get<BestmileDatabase>().getBestmileDao() }

    single { BestmileRetrofitClient() }

    factory { get<BestmileRetrofitClient>().getStationsService() }

    single<DataSyncManager> {
        DataSyncManagerImpl(
            stationsService = get(),
            bestmileDao = get()
        )
    }

    single { DirectionsRetrofitClient() }

    factory { get<DirectionsRetrofitClient>().getDirectionsService() }

    single<VehicleManager> {
        VehicleManagerImpl(
            bestmileDao = get(),
            directionsService = get(),
            markersColorUtils = get()
        )
    }
}

private const val BESTMILE_DATABASE = "bestmile"
