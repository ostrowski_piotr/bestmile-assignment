package com.bestmile.assignment

import android.app.Application
import com.bestmile.assignment.di.modules.appModule
import com.bestmile.assignment.di.modules.dataModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
class AssignmentApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@AssignmentApplication)
            modules(
                listOf(
                    appModule,
                    dataModule
                )
            )
        }
    }
}
