package com.bestmile.assignment.views.map

/**
 * Created by Piotr Ostrowski on 28/11/2018.
 */
data class MapMarkerSelection(val stationId: Int, val selected: Boolean)