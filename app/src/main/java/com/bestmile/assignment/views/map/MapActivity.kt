package com.bestmile.assignment.views.map

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.activity.viewModels
import com.bestmile.assignment.R
import com.bestmile.assignment.base.AbstractViewModelActivity
import com.bestmile.assignment.databinding.ActivityMapBinding
import com.bestmile.assignment.managers.Vehicle
import com.bestmile.assignment.persistance.models.StationModel
import com.bestmile.assignment.utils.MarkersColorUtils
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.PolyUtil
import io.reactivex.android.schedulers.AndroidSchedulers
import org.koin.android.ext.android.inject
import java.util.*

/**
 * Created by Piotr Ostrowski on 27/11/2018.
 */
class MapActivity : AbstractViewModelActivity<MapViewModel, ActivityMapBinding>() {

    private val markersColorUtils by inject<MarkersColorUtils>()

    override val viewModel: MapViewModel by viewModels()
    override val layoutResId = R.layout.activity_map

    private var googleMap: GoogleMap? = null
    private val stationMarkersMap = TreeMap<Int, Marker>()
    // TODO: Merge into one data class
    private val vehicleMarkersMap = TreeMap<Int, Marker>()
    private val vehiclePolylineMap = TreeMap<Int, Polyline>()
    private var showAddVehicleAction = false

    private val markerClickListener = GoogleMap.OnMarkerClickListener { marker ->
        val stationUid = marker.tag as? Int
        stationUid?.let { viewModel.onMarkerClick(it) }
        return@OnMarkerClickListener false
    }
    private val mapClickListener = GoogleMap.OnMapClickListener {
        viewModel.onMapClick()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.startUpdatingTotalTime()

        val fragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        fragment?.getMapAsync {
            it.setOnMarkerClickListener(markerClickListener)
            it.setOnMapClickListener(mapClickListener)
            googleMap = it

            subscribeToObservables()
            viewModel.onMapReady(savedInstanceState != null)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_map, menu)
        menu?.findItem(R.id.action_add_vehicle)?.isVisible = showAddVehicleAction
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_add_vehicle) {
            viewModel.onAddVehicleClick()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun subscribeToObservables() {
        disposable {
            viewModel.cameraUpdateObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::updateCamera)
        }
        disposable {
            viewModel.stationMarkersObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::displayStationMarkers)
        }
        disposable {
            viewModel.stationMarkerToggleObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::toggleMarker)
        }
        disposable {
            viewModel.displayErrorObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::displayError)
        }
        disposable {
            viewModel.vehicleRemovalObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::removeVehicle)
        }
        disposable {
            viewModel.vehicleMovementObservable()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::moveVehicle)
        }
    }

    private fun updateCamera(update: MapCameraUpdate) {
        val cameraUpdate = when (update) {
            is MapCameraUpdate.PointZoomUpdate -> CameraUpdateFactory.newLatLngZoom(update.latLng, update.zoom)
            is MapCameraUpdate.AnimatedPointUpdate -> CameraUpdateFactory.newLatLng(update.latLng)
            is MapCameraUpdate.BoundsUpdate -> CameraUpdateFactory.newLatLngBounds(update.latLngBounds, update.offset)
        }

        if (update.isAnimated()) {
            googleMap?.animateCamera(cameraUpdate)
        } else {
            googleMap?.moveCamera(cameraUpdate)
        }
    }

    private fun displayStationMarkers(stations: List<StationModel>) {
        googleMap?.also { map ->
            stations.forEach {
                val markerOptions = MarkerOptions()
                    .position(LatLng(it.latitude, it.longitude))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE))
                stationMarkersMap[it.uid] = map.addMarker(markerOptions).apply { tag = it.uid }
            }
        }
    }

    private fun toggleMarker(selection: MapMarkerSelection) {
        val color = if (selection.selected) {
            BitmapDescriptorFactory.HUE_RED
        } else {
            BitmapDescriptorFactory.HUE_BLUE
        }
        stationMarkersMap[selection.stationId]?.setIcon(BitmapDescriptorFactory.defaultMarker(color))
        showAddVehicleAction = selection.selected
        invalidateOptionsMenu()
    }

    private fun moveVehicle(movement: Vehicle.Movement) {
        googleMap?.also { map ->
            if (vehicleMarkersMap.containsKey(movement.vehicleId)) {
                val marker = vehicleMarkersMap[movement.vehicleId]
                marker?.position = movement.currentPosition
            } else {
                val markerBitmap = BitmapDescriptorFactory.fromBitmap(
                    markersColorUtils.getTintedBitmap(resources, R.drawable.ic_directions_bus, movement.color)
                )
                val markerOptions = MarkerOptions()
                    .position(movement.currentPosition)
                    .icon(markerBitmap)
                vehicleMarkersMap[movement.vehicleId] = map.addMarker(markerOptions).apply { tag = movement.vehicleId }
            }

            vehiclePolylineMap[movement.vehicleId]?.remove()
            val options = PolylineOptions()
                .width(POLYLINE_WIDTH)
                .color(movement.color)
            movement.polylineLeft.forEach {
                options.addAll(PolyUtil.decode(it))
            }
            vehiclePolylineMap[movement.vehicleId] = map.addPolyline(options)
        }
    }

    private fun removeVehicle(vehicleId: Int) {
        vehicleMarkersMap[vehicleId]?.remove()
        vehiclePolylineMap[vehicleId]?.remove()
    }

    private companion object {
        const val POLYLINE_WIDTH = 10.0f
    }
}
