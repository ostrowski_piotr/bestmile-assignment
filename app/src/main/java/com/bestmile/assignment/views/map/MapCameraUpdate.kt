package com.bestmile.assignment.views.map

import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds

/**
 * Created by Piotr Ostrowski on 28/11/2018.
 */
sealed class MapCameraUpdate {
    data class PointZoomUpdate(val latLng: LatLng, val zoom: Float) : MapCameraUpdate() {
        override fun isAnimated() = false
    }

    data class AnimatedPointUpdate(val latLng: LatLng) : MapCameraUpdate() {
        override fun isAnimated() = true
    }

    data class BoundsUpdate(val latLngBounds: LatLngBounds, val offset: Int) : MapCameraUpdate() {
        override fun isAnimated() = false
    }

    abstract fun isAnimated(): Boolean
}