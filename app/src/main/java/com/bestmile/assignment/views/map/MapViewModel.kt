package com.bestmile.assignment.views.map

import androidx.databinding.ObservableField
import android.util.Log
import com.bestmile.assignment.R
import com.bestmile.assignment.base.AbstractRxViewModel
import com.bestmile.assignment.base.helpers.ErrorStyle
import com.bestmile.assignment.persistance.BestmileDao
import com.bestmile.assignment.persistance.models.StationModel
import com.bestmile.assignment.managers.DataSyncManager
import com.bestmile.assignment.managers.Vehicle
import com.bestmile.assignment.managers.VehicleManager
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import org.koin.core.inject
import java.util.ArrayList
import java.util.concurrent.TimeUnit

/**
 * Created by Piotr Ostrowski on 27/11/2018.
 */
class MapViewModel : AbstractRxViewModel() {

    private val dataSyncManager by inject<DataSyncManager>()
    private val vehicleManager by inject<VehicleManager>()
    private val bestmileDao by inject<BestmileDao>()

    val vehiclesCreated = ObservableField(0)
    val vehiclesActive = ObservableField(0)
    val totalTime = ObservableField(0L)

    private val cameraUpdateSubject: PublishSubject<MapCameraUpdate> = PublishSubject.create()
    private val stationMarkersSubject: BehaviorSubject<List<StationModel>> = BehaviorSubject.create()
    private val stationMarkerToggleSubject: BehaviorSubject<MapMarkerSelection> = BehaviorSubject.create()
    private val vehicleMovementSubject: PublishSubject<Vehicle.Movement> = PublishSubject.create()
    private val vehicleRemovalSubject: BehaviorSubject<Int> = BehaviorSubject.create()
    private var totalTimeDisposable: Disposable? = null

    private var selectedStationId: Int? = null
    private val vehiclesFleet = ArrayList<Vehicle>(FLEET_SIZE)
    // Time spent by vehicles which reached their destination
    private var totalCompletedTimeSpent: Long = 0

    fun startUpdatingTotalTime() {
        if (totalTimeDisposable != null) {
            return
        }
        totalTimeDisposable = Observable.interval(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { calculateTotalTime() }

        totalTimeDisposable?.also {
            disposable { it }
        }
    }

    fun onMapReady(afterScreenRotation: Boolean) {
        if (!afterScreenRotation) {
            val latLng = LatLng(LAUSANNE_LATITUDE, LAUSANNE_LONGITUDE)
            val cameraUpdate = MapCameraUpdate.PointZoomUpdate(latLng, INITIAL_ZOOM_LEVEL)
            cameraUpdateSubject.onNext(cameraUpdate)
            fetchStations()
        }
        vehiclesFleet.forEach { vehicleMovementSubject.onNext(it.getCurrentMovement()) }
    }

    fun onMarkerClick(stationId: Int) {
        selectedStationId?.also {
            stationMarkerToggleSubject.onNext(MapMarkerSelection(it, false))
        }
        stationMarkerToggleSubject.onNext(MapMarkerSelection(stationId, true))
        selectedStationId = stationId
    }

    fun onMapClick() {
        selectedStationId?.also {
            stationMarkerToggleSubject.onNext(MapMarkerSelection(it, false))
        }
        selectedStationId = null
    }

    fun onAddVehicleClick() {
        if (vehiclesFleet.size >= FLEET_SIZE) {
            displayErrorSubject.onNext(ErrorStyle.ToastErrorStyle(R.string.our_fleet_is_out))
            return
        }

        selectedStationId?.also {
            disposable {
                vehicleManager.createVehicleForTargetStation(it)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ vehicle ->
                        // TODO: Handle concurrency with @Synchronized?
                        vehiclesFleet.add(vehicle)
                        vehiclesCreated.set(vehiclesCreated.get()?.plus(1))
                        vehiclesActive.set(vehiclesFleet.size)
                        startVehicle(vehicle)
                    }, { error ->
                        Log.v(TAG, "Vehicle creation failed due to ${error.message}", error)
                        displayErrorSubject.onNext(ErrorStyle.AlertDialogErrorStyle(R.string.vehicle_creation_failed))
                    })
            }
        }
    }

    fun cameraUpdateObservable(): Observable<MapCameraUpdate> = cameraUpdateSubject.hide()
    fun stationMarkersObservable(): Observable<List<StationModel>> = stationMarkersSubject.hide()
    fun stationMarkerToggleObservable(): Observable<MapMarkerSelection> = stationMarkerToggleSubject.hide()
    fun vehicleMovementObservable(): Observable<Vehicle.Movement> = vehicleMovementSubject.hide()
    fun vehicleRemovalObservable(): Observable<Int> = vehicleRemovalSubject.hide()

    private fun fetchStations() {
        // TODO: Add progress indicator
        disposable {
            dataSyncManager.synchronizeStations()
                .andThen(bestmileDao.getAllStations())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ stations ->
                    stationMarkersSubject.onNext(stations)
                    getStationsCameraUpdate(stations)?.let {
                        cameraUpdateSubject.onNext(it)
                    }
                }, { error ->
                    Log.e(TAG, "Failed to synchronize stations data.", error)
                    displayErrorSubject.onNext(ErrorStyle.AlertDialogErrorStyle(R.string.stations_sync_failed))
                })
        }
    }

    private fun getStationsCameraUpdate(stations: List<StationModel>): MapCameraUpdate.BoundsUpdate? {
        if (stations.isEmpty()) {
            return null
        }

        val latLngBuilder = LatLngBounds.builder()
        stations.forEach {
            latLngBuilder.include(LatLng(it.latitude, it.longitude))
        }
        return MapCameraUpdate.BoundsUpdate(latLngBuilder.build(), BOUNDS_OFFSET_DISTANCE)
    }

    private fun startVehicle(vehicle: Vehicle) {
        disposable {
            vehicle.getMovementObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    vehicleMovementSubject.onNext(it)
                }, {
                    // We are not expecting any errors here
                }, {
                    vehicleRemovalSubject.onNext(vehicle.vehicleId)
                    vehiclesFleet.remove(vehicle)
                    vehiclesActive.set(vehiclesFleet.size)
                    totalCompletedTimeSpent += calculateTimeSpentSince(vehicle.creationTimestamp)
                })
        }
    }

    private fun calculateTotalTime() {
        var total = totalCompletedTimeSpent
        vehiclesFleet.forEach { total += calculateTimeSpentSince(it.creationTimestamp) }
        totalTime.set(total)
    }

    private fun calculateTimeSpentSince(startTimestamp: Long) = System.currentTimeMillis() - startTimestamp

    private companion object {
        val TAG = MapViewModel::class.java.simpleName
        const val LAUSANNE_LATITUDE = 46.519962
        const val LAUSANNE_LONGITUDE = 6.633597
        const val INITIAL_ZOOM_LEVEL = 10.0f
        const val BOUNDS_OFFSET_DISTANCE = 200
        const val FLEET_SIZE = 10
    }
}
