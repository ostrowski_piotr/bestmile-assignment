package com.bestmile.assignment.utils

import android.content.res.Resources
import android.graphics.*
import androidx.annotation.ColorInt
import androidx.annotation.DrawableRes
import java.util.*

/**
 * Created by Piotr Ostrowski on 01/12/2018.
 */
class MarkersColorUtils {

    fun generateRandomColor(): Int {
        val rand = Random()
        return Color.rgb(
            rand.nextInt(MAX_COLOR_VALUE),
            rand.nextInt(MAX_COLOR_VALUE),
            rand.nextInt(MAX_COLOR_VALUE)
        )
    }

    fun getTintedBitmap(resources: Resources, @DrawableRes drawableResId: Int, @ColorInt color: Int): Bitmap {
        val origin = BitmapFactory.decodeResource(resources, drawableResId)
        val destination = Bitmap.createBitmap(origin.width, origin.height, Bitmap.Config.ARGB_8888)
        val paint = Paint().apply {
            colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
        }
        Canvas(destination).apply {
            drawBitmap(origin, 0.0f, 0.0f, paint)
        }
        return destination
    }

    companion object {
        private const val MAX_COLOR_VALUE = 155 // 255 was too bright in some cases
    }
}
