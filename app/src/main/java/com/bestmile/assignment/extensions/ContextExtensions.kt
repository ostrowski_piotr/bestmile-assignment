package com.bestmile.assignment.extensions

import android.content.Context
import android.widget.Toast

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */

/**
 * Displays long Toast for given resId.
 */
fun Context.longToast(stringResId: Int) {
    Toast.makeText(this, stringResId, Toast.LENGTH_LONG).show()
}

/**
 * Displays short Toast for given resId.
 */
fun Context.shortToast(stringResId: Int) {
    Toast.makeText(this, stringResId, Toast.LENGTH_SHORT).show()
}

/**
 * Displays short Toast for given String.
 */
fun Context.shortToast(string: String) {
    Toast.makeText(this, string, Toast.LENGTH_SHORT).show()
}