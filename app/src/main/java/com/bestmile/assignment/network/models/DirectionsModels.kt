package com.bestmile.assignment.network.models

import com.google.android.gms.maps.model.LatLng
import com.google.gson.annotations.SerializedName

/**
 * Created by Piotr Ostrowski on 29.11.2018.
 */
data class DirectionsModel(
    val status: String,
    val routes: List<RouteModel>
)

data class RouteModel(
    val summary: String,
    val legs: List<LegsModel>,
    @SerializedName("overview_polyline")
    val polyline: PolylineModel
)

data class LegsModel(
    @SerializedName("start_location")
    val start: LocationModel,
    @SerializedName("end_location")
    val end: LocationModel,
    val steps: List<StepsModel>
)

data class PolylineModel(val points: String)

data class StepsModel(
    @SerializedName("start_location")
    val start: LocationModel,
    @SerializedName("end_location")
    val end: LocationModel,
    val polyline: PolylineModel
)

data class LocationModel(val lat: Double, val lng: Double) {
    fun toLatLng() = LatLng(lat, lng)
}