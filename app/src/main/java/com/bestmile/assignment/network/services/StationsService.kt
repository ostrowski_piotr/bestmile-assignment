package com.bestmile.assignment.network.services

import com.bestmile.assignment.network.models.ListContainerModel
import com.bestmile.assignment.persistance.models.StationModel
import io.reactivex.Single
import retrofit2.http.GET

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
interface StationsService {

    @GET("stations")
    fun getStations(): Single<ListContainerModel<StationModel>>
}