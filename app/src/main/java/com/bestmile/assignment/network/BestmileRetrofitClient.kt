package com.bestmile.assignment.network

import com.bestmile.assignment.BuildConfig
import com.bestmile.assignment.network.services.StationsService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
open class BestmileRetrofitClient : BaseRetrofitClient() {

    fun getStationsService(): StationsService = retrofit.create(StationsService::class.java)

    override fun provideBaseURL() = BuildConfig.API_BASE_URL

    override fun configureRetrofit(retrofitBuilder: Retrofit.Builder) {
        // nop
    }
}
