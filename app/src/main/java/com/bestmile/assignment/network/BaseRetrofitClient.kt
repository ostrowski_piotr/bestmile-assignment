package com.bestmile.assignment.network

import com.bestmile.assignment.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Piotr Ostrowski on 28/11/2018.
 */
abstract class BaseRetrofitClient {

    protected var retrofit: Retrofit

    init {
        val httpClientBuilder = OkHttpClient.Builder()
        httpClientBuilder.connectTimeout(CONNECTION_TIMEOUT_TIME, TimeUnit.SECONDS)
        httpClientBuilder.readTimeout(CONNECTION_TIMEOUT_TIME, TimeUnit.SECONDS)
        httpClientBuilder.writeTimeout(CONNECTION_TIMEOUT_TIME, TimeUnit.SECONDS)

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addInterceptor(loggingInterceptor)
        }

        val retrofitBuilder = Retrofit.Builder()
            .client(httpClientBuilder.build())
            .baseUrl(this.provideBaseURL())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        this.configureRetrofit(retrofitBuilder)

        retrofit = retrofitBuilder.build()
    }

    protected abstract fun provideBaseURL(): String

    protected abstract fun configureRetrofit(retrofitBuilder: Retrofit.Builder)

    companion object {
        private const val CONNECTION_TIMEOUT_TIME = 60L
    }
}