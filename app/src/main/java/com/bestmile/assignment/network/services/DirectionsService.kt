package com.bestmile.assignment.network.services

import com.bestmile.assignment.network.models.DirectionsModel
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Piotr Ostrowski on 28/11/2018.
 */
interface DirectionsService {

    @GET("json")
    fun getDirections(
        @Query("origin") origin: LatLng,
        @Query("destination") destination: LatLng,
        @Query("key") apiKey: String
    ): Single<DirectionsModel>
}