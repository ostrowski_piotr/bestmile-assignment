package com.bestmile.assignment.network

import com.bestmile.assignment.BuildConfig
import com.bestmile.assignment.network.services.DirectionsService
import com.google.android.gms.maps.model.LatLng
import retrofit2.Converter
import retrofit2.Retrofit
import java.lang.reflect.Type

/**
 * Created by Piotr Ostrowski on 28/11/2018.
 */
class DirectionsRetrofitClient : BaseRetrofitClient() {

    fun getDirectionsService(): DirectionsService = retrofit.create(DirectionsService::class.java)

    override fun provideBaseURL() = BuildConfig.DIRECTIONS_BASE_URL

    override fun configureRetrofit(retrofitBuilder: Retrofit.Builder) {
        retrofitBuilder.addConverterFactory(LatLngConverterFactory())
    }

    class LatLngConverterFactory : Converter.Factory() {

        override fun stringConverter(type: Type, annotations: Array<Annotation>, retrofit: Retrofit): Converter<*, String>? {
            return if (type != LatLng::class.java) {
                super.stringConverter(type, annotations, retrofit)
            } else {
                Converter<LatLng, String> { latLng ->
                    "${latLng.latitude},${latLng.longitude}"
                }
            }
        }
    }
}
