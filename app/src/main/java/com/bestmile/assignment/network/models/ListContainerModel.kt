package com.bestmile.assignment.network.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Piotr Ostrowski on 28/11/2018.
 */
data class ListContainerModel<T>(
    @SerializedName("stations")
    var entities: List<T>,
    var total: Int,
    var error: Boolean,
    var status: Int
) where T : AbstractModel