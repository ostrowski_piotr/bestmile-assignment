package com.bestmile.assignment.managers

import androidx.annotation.ColorInt
import com.bestmile.assignment.BuildConfig
import com.bestmile.assignment.network.models.DirectionsModel
import com.bestmile.assignment.network.models.StepsModel
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * Created by Piotr Ostrowski on 29.11.2018.
 */
class Vehicle(
    val vehicleId: Int,
    @ColorInt val color: Int,
    directions: DirectionsModel
) {

    private var remainingRoute: LinkedList<StepsModel>
    val creationTimestamp: Long

    init {
        // I'm assuming here that we will always have a route available so routes won't be empty.
        // In addition I'm not providing any waypoints so I'm also assuming that API will return only one leg
        val leg = directions.routes.first().legs.first()
        remainingRoute = LinkedList()
        remainingRoute.addAll(leg.steps)
        creationTimestamp = System.currentTimeMillis()
    }

    fun getCurrentMovement(): Movement {
        val currentStep = remainingRoute.peek()
        val currentPosition = currentStep.start.toLatLng()
        val polyline = ArrayList<String>()
        remainingRoute.forEach { polyline.add(it.polyline.points) }
        return Movement(vehicleId, color, currentPosition, polyline)
    }

    fun getMovementObservable(): Observable<Movement> =
        Observable.interval(BuildConfig.VEHICLE_MOVEMENT_INTERVAL, TimeUnit.SECONDS)
            .startWith(FIRST_ITEM_INDEX)
            .takeWhile { remainingRoute.isNotEmpty() }
            .doAfterNext { remainingRoute.poll() }
            .map { getCurrentMovement() }

    data class Movement(
        val vehicleId: Int,
        @ColorInt val color: Int,
        val currentPosition: LatLng,
        val polylineLeft: List<String>
    )

    private companion object {
        const val FIRST_ITEM_INDEX = 0L
    }
}
