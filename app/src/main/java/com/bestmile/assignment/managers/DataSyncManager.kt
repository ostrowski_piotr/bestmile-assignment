package com.bestmile.assignment.managers

import com.bestmile.assignment.network.services.StationsService
import com.bestmile.assignment.persistance.BestmileDao
import com.bestmile.assignment.persistance.models.StationModel
import io.reactivex.Completable

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
interface DataSyncManager {
    fun synchronizeStations(): Completable
}

class DataSyncManagerImpl(
    private val stationsService: StationsService,
    private val bestmileDao: BestmileDao
) : DataSyncManager {

    override fun synchronizeStations() = stationsService.getStations()
        .flatMapCompletable { stations -> storeStations(stations.entities) }

    // This is just a very very basic example of data sync. Normally I would pull the difference delta
    // then update, delete appropriate rows and insert new ones.
    private fun storeStations(stations: List<StationModel>) = Completable.fromAction {
        bestmileDao.deleteAllStations()
        bestmileDao.insertStations(stations)
    }
}
