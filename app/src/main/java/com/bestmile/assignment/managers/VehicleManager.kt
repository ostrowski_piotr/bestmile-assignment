package com.bestmile.assignment.managers

import com.bestmile.assignment.BuildConfig
import com.bestmile.assignment.network.services.DirectionsService
import com.bestmile.assignment.persistance.BestmileDao
import com.bestmile.assignment.persistance.models.StationModel
import com.bestmile.assignment.utils.MarkersColorUtils
import com.google.android.gms.maps.model.LatLng
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers

/**
 * Created by Piotr Ostrowski on 29.11.2018.
 */
interface VehicleManager {
    fun createVehicleForTargetStation(targetStationId: Int): Single<Vehicle>
}

class VehicleManagerImpl(
    private val bestmileDao: BestmileDao,
    private val directionsService: DirectionsService,
    private val markersColorUtils: MarkersColorUtils
) : VehicleManager {

    override fun createVehicleForTargetStation(targetStationId: Int) =
        generateStationPoints(targetStationId)
            .flatMap {
                val origin = LatLng(it.first.latitude, it.first.longitude)
                val destination = LatLng(it.second.latitude, it.second.longitude)
                directionsService.getDirections(origin, destination, BuildConfig.DIRECTIONS_API_KEY)
            }
            .map { directions ->
                Vehicle(
                    vehicleId = idGenerator++,
                    color = markersColorUtils.generateRandomColor(),
                    directions = directions
                )
            }

    private fun generateStationPoints(targetStationId: Int): Single<Pair<StationModel, StationModel>> =
        Single.zip(
            bestmileDao.getOtherRandomStation(targetStationId),
            bestmileDao.getStation(targetStationId),
            BiFunction { startingPoint, endingPoint -> Pair(startingPoint, endingPoint) }
        )

    companion object {
        private var idGenerator: Int = 1
    }
}
