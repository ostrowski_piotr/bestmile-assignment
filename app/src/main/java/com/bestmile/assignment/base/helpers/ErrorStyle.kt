package com.bestmile.assignment.base.helpers

import androidx.annotation.StringRes

/**
 * Created by Piotr Ostrowski on 01/12/2018.
 */
sealed class ErrorStyle {
    data class AlertDialogErrorStyle(@StringRes val reason: Int) : ErrorStyle()
    data class ToastErrorStyle(@StringRes val reason: Int) : ErrorStyle()
}
