package com.bestmile.assignment.base

import androidx.databinding.BindingAdapter
import android.widget.TextView
import com.bestmile.assignment.R
import java.util.concurrent.TimeUnit

/**
 * Created by Piotr Ostrowski on 30/11/2018.
 */
@BindingAdapter("valueText")
fun setTextViewValueBinding(view: TextView, valueText: Int?) {
    view.text = valueText?.toString() ?: EMPTY_STRING
}

@BindingAdapter("timestampText")
fun setTextViewTimestampBinding(view: TextView, millis: Long?) {
    view.text = with(view.context) {
        millis?.let {
            getString(
                R.string.timestamp_text_format,
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))
            )
        } ?: getString(R.string.timestamp_text_empty)
    }
}

private const val EMPTY_STRING = ""
