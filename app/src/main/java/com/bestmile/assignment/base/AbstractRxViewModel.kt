package com.bestmile.assignment.base

import androidx.lifecycle.ViewModel
import com.bestmile.assignment.base.helpers.ErrorStyle
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import org.koin.core.KoinComponent

/**
 * Created by Piotr Ostrowski on 27/11/2018.
 */
abstract class AbstractRxViewModel : ViewModel(), KoinComponent {

    private val disposables: CompositeDisposable = CompositeDisposable()
    protected val displayErrorSubject: PublishSubject<ErrorStyle> = PublishSubject.create()

    override fun onCleared() {
        super.onCleared()
        disposables.clear()
    }

    fun displayErrorObservable(): Observable<ErrorStyle> = displayErrorSubject.hide()

    protected fun disposable(disposableProvider: () -> Disposable) {
        disposables += disposableProvider()
    }
}
