package com.bestmile.assignment.base

import androidx.lifecycle.ViewModel
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import android.os.Bundle
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bestmile.assignment.BR
import com.bestmile.assignment.R
import com.bestmile.assignment.base.helpers.ErrorStyle
import com.bestmile.assignment.extensions.shortToast
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import org.koin.android.ext.android.inject

/**
 * Created by Piotr Ostrowski on 27/11/2018.
 */
abstract class AbstractViewModelActivity<V : ViewModel, B : ViewDataBinding> : AppCompatActivity() {

    protected abstract val layoutResId: Int
    protected abstract val viewModel: V
    protected lateinit var binding: B

    private val disposables by inject<CompositeDisposable>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutResId)

        val variable = binding.setVariable(BR.viewModel, viewModel)
        if (!variable) {
            throw IllegalArgumentException("Binding layout must have variable named 'viewModel'")
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposables.clear()
    }

    protected fun disposable(disposableProvider: () -> Disposable) {
        disposables += disposableProvider()
    }

    protected fun displayError(errorStyle: ErrorStyle) {
        when (errorStyle) {
            is ErrorStyle.AlertDialogErrorStyle -> showAlertDialogError(errorStyle.reason)
            is ErrorStyle.ToastErrorStyle -> shortToast(errorStyle.reason)
        }

    }

    private fun showAlertDialogError(@StringRes reason: Int) {
        AlertDialog.Builder(this)
            .setTitle(R.string.error)
            .setMessage(reason)
            .setPositiveButton(R.string.ok) { _, _ -> }
            .show()
    }
}
