package com.bestmile.assignment.persistance.models

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.bestmile.assignment.network.models.AbstractModel

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
@Entity(tableName = "stations")
data class StationModel(
    @PrimaryKey var uid: Int,
    @ColumnInfo(name = "latitude") var latitude: Double,
    @ColumnInfo(name = "longitude") var longitude: Double,
    @ColumnInfo(name = "station_name") var stationName: String?
) : AbstractModel()
