package com.bestmile.assignment.persistance

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.bestmile.assignment.persistance.models.StationModel
import io.reactivex.Single

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
@Dao
interface BestmileDao {

    @Insert
    fun insertStations(stations: List<StationModel>): List<Long>

    @Query("SELECT * FROM stations")
    fun getAllStations(): Single<List<StationModel>>

    @Query("DELETE FROM stations")
    fun deleteAllStations()

    @Query("SELECT * FROM stations WHERE uid = :stationId")
    fun getStation(stationId: Int): Single<StationModel>

    @Query("SELECT * FROM stations WHERE uid != :stationId ORDER BY RANDOM() LIMIT 1")
    fun getOtherRandomStation(stationId: Int): Single<StationModel>
}
