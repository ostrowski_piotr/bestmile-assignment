package com.bestmile.assignment.persistance

import androidx.room.Database
import androidx.room.RoomDatabase
import com.bestmile.assignment.persistance.models.StationModel

/**
 * Created by Piotr Ostrowski on 27.11.2018.
 */
@Database(entities = [StationModel::class], version = 1, exportSchema = false)
abstract class BestmileDatabase : RoomDatabase() {
    abstract fun getBestmileDao(): BestmileDao
}
